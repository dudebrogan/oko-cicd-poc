# Notes for anyone reading this

## How it works

Login with magic link. If it is your first time logging in, then you'll need to add your name.

After that, you can go to the dashboard to change your company type. For the sake of this demo, it's not very important except to show that there are a different set of docs for them.

On the transactions page, you can click on a transaction to preview it. You can download the file or upload more files as well.

On the documents page, you can view all documents you've uploaded. You can upload more with a valid transaction id or download documents. Here, you can see an image preview of the pdf.

Wherever you upload, you will get a result from a pdf text scan (utilizing Google Vision AI) showing the word count, longest word, and shortest word.

I am not a designer, so the stuff that I added without a mockup is less than impressive visually. But, I can follow any design direction pretty easily. I also didn't have time to worry much about mobile optimization, so don't judge too harshly there. 
You have to do or skip the documents tutorial and transactions tutorial to not automatically get the onboarding tour. You can reset it on the dashboard if you want to look at it again

## Next Steps

There are definitely some optimizations I would have made with more time and would make next, such as setting up services for http requests instead of having it in the store, better global styling, and refactoring some components. There is some repeated code simple because of time restraints. I also would need to dig into some better env variables insertion, like doppler, because of gcp I had to hardcode a few things. More tests and loading icons for sure. Simple stuff, just lower priority for this purpose. 

## Architecture

This runs through a gitlab pipeline then automatically deploys if all is well. It is served by cloud run.

The API is a simple node/express server. The document scanning happens there, not the frontend. I would say we probably want to, at some point, migrate things to cloud functions, but it's more cost effect before scaling to just keep it simple.

The DB is a Google Cloud MySQL database
