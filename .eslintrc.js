module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    semi: [2, 'never'],
    quotes: [2, 'single'],
    'vue/no-multiple-template-root': 'off',
    'object-curly-spacing': [2, 'always'],
    'no-unused-vars': [1, { 'vars': 'all', 'args': 'after-used', 'ignoreRestSiblings': false }],
    'vue/no-v-model-argument': 'off' ,
    'vue/no-parsing-error': 'off'
  }
}

