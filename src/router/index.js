import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from '@/views/Dashboard.vue'
import Auth from '../views/Auth'
import store from '@/store'
import Transactions from '@/views/Transactions'
import Documents from '@/views/Documents'
import Signup from '@/views/Signup'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Dashboard,
    meta: { requiresUserInfo: false, requiresAuth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: Auth,
    meta: { disallowedWhenAuth: '/' },
  },
  {
    path: '/completeSignup',
    name: 'Signup',
    component: Signup,
    meta: { alwaysAllow: true },
  },
  {
    path: '/transactions',
    name: 'Transactions',
    component: Transactions,
    meta: { requiresAuth: true },
  },
  {
    path: '/documents',
    name: 'Documents',
    component: Documents,
    meta: { requiresAuth: true },
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach(async (to, from, next) => {
  const userAuth = store.state.auth.userAuth

  const token = store.state.auth.userAuth?.issuer
  let userInfo = store.state.auth.userInfo
  const loggedIn = token !== null && token !== undefined
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const disallowedWhenAuth = to.matched.some(record => record.meta.disallowedWhenAuth)
  const requiresUserInfo = to.matched.some(record => record.meta.requiresUserInfo)
  const alwaysAllow = to.matched.some(record => record.meta.alwaysAllow)

  if (alwaysAllow) {
    next()
    return
  }

  //TODO: Map out routing logic and refactor this.

  if (!loggedIn && to.path==='/login') next()
  else if ((requiresAuth && !loggedIn)) {
    next('/login')
  }
  else if (!userInfo?.id) {

    if (userAuth?.email) await store.dispatch('auth/getUserStatus', {
      email: userAuth.email
    })

    userInfo = store.state.auth.userInfo

    if (!userInfo?.id) {
      next('/completeSignup')
      return
    }
    userInfo = store.state.auth.userInfo

    if (!userInfo?.companyId && requiresUserInfo) {
      next('/')
      return
    }
    next()
  }
  else if (disallowedWhenAuth && loggedIn) next(disallowedWhenAuth)
  else next()
})

export default router
