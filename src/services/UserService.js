import axios from 'axios'
import { Magic } from 'magic-sdk'

const apiLink = process.env.VUE_APP_API_ENDPOINT ?? 'https://oko-api-ezxw6bsbyq-uc.a.run.app'
const magicKey = new Magic(process.env.VUE_APP_MAGIC_API_KEY || 'pk_live_37A270F0A4C38362')
// obviously this isn't right but google cloud run is super obnoxious with front end secrets
// the answer is to use doppler, it's literally a dev's dream for env management

class UserService {
  async login(email) {
    await magicKey.auth.loginWithMagicLink({ email })
    return await magicKey.user.getMetadata()
  }

  async logout() {
    return magicKey.user.logout()
  }

  async completeSignup(fullName, email) {
    return await axios.post(`${apiLink}/completeSignup`, {
      fullName,
      email
    })
  }

  async getUserStatus(email) {
    const userData = await axios.get(`${apiLink}/getUserStatus`, {
      params: {
        email
      }
    })

    return userData.data
  }

  async becomeCompanyType(id, companyType) {
    return await axios.post(`${apiLink}/become`, {
      id: id,
      type: companyType
    })
  }

  async updateUserTutorial(id, tutorialCompleted, completedTransactionsTutorial, completedDocumentsTutorial) {
    await axios.post(`${apiLink}/updateusertutorial`, {
      id,
      tutorialCompleted: tutorialCompleted === false ? false : (completedTransactionsTutorial && completedDocumentsTutorial)
    })
  }
}

export default new UserService()
