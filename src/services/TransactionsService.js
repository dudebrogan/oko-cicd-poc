import axios from 'axios'
import { DateTime } from 'luxon'

const apiLink = process.env.VUE_APP_API_ENDPOINT ?? 'https://oko-api-ezxw6bsbyq-uc.a.run.app'

class TransactionsService {
  async getTransactions(companyId, transactionType) {
    const data = await axios.get(`${apiLink}/transactions`, {
      params: {
        companyId,
        transactionType
      }
    })

    return data.data?.transactions?.map(transaction => {
      transaction.created = DateTime.fromISO(transaction.created).toFormat('MM/dd/yy')
      return transaction
    })
  }
}

export default new TransactionsService()
