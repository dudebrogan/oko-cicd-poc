import axios from 'axios'

const apiLink = process.env.VUE_APP_API_ENDPOINT ?? 'https://oko-api-ezxw6bsbyq-uc.a.run.app'


const http = axios.create({
  baseURL: apiLink,
  headers: {
    'Content-type': 'application/json'
  }
})
class UploadFilesService {
  upload(file, params, onUploadProgress) {
    let formData = new FormData()

    formData.append('file', file)

    return http.post('/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress
    })
  }

  async saveFilePath(config) {
    try {
      const res = await http.post('/saveFile', config)
      const { data } = res
      const longestWord = data.reduce((a, b) => a.length < b.length ? b : a, '')
      const shortestWord = data.reduce((a, b) => a.length <= b.length ? a : b)
      window.alert(`You have uploaded a file to Oko. There were ${data.length} words found, with ${longestWord} being the longest and ${shortestWord} being the shortest`)
      return res
    } catch (e) {
      return e.response.data.error
    }
  }
  getFiles(userId) {
    return http.get('/files', {
      params: {
        userId
      }
    })
  }

  async getFile(name) {
    try {
      let response = await http.get(`/files/${name}`, { responseType: 'blob' })
      const blob = new Blob([response.data], { type: 'application/pdf' })
      const link = document.createElement('a')
      link.href = URL.createObjectURL(blob)
      link.download = name
      link.click()
      URL.revokeObjectURL(link.href)
    } catch(e) {
      console.error('get file error: ', e)
    }


  }
}

export default new UploadFilesService()
