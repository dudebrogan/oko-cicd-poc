import router from '../router'
import UserService from '@/services/UserService'

export default {
  namespaced: true,
  state: {
    userAuth: null,
    userInfo: {
      companyId: '',
      id: '',
      emailAddress: '',
      firstName: '',
      lastName: '',
      tutorialCompleted: true
    },
    accessToken: null,
    isProcessingLogin: false,
    completedDocumentsTutorial: false,
    completedTransactionsTutorial: false,
    showNameForm: false,
  },
  mutations: {
    setUserAuth(state, user) {
      state.userAuth = user
    },
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    setAccessToken(state, token) {
      state.accessToken = token
    },
    setIsProcessingLogin(state, isProcessingLogin) {
      state.isProcessingLogin = isProcessingLogin
    },
    setCompletedDocumentsTutorial(state, completed) {
      state.completedDocumentsTutorial = completed
    },
    setCompletedTransactionsTutorial(state, completed) {
      state.completedTransactionsTutorial = completed
    },
  },
  actions: {
    async login({ commit, dispatch, state }, payload) {
      commit('setIsProcessingLogin', true)
      try {
        const data = await UserService.login(payload.email)
        commit('setUserAuth', data)
        commit('setAccessToken', data.issuer)
        await dispatch('getUserStatus', payload)
        await router.push('/')
      } catch (e) {
        console.error('login error', e)
      } finally {
        commit('setIsProcessingLogin', false)
      }
    },
    async completeSignup({ commit }, payload) {
      commit('setIsProcessingLogin', true)

      try {
        const userInfo = await UserService.completeSignup(payload.fullName, payload.email)
        commit('setUserInfo', userInfo?.data)
        await router.push({ name: 'Home' })
      } catch (e) {
        console.error(e)
      } finally {
        commit('setIsProcessingLogin', false)
      }
    },
    async logout({ commit }) {
      await UserService.logout()
      commit('setUserAuth', null)
      commit('setAccessToken', null)
      commit('setUserInfo', null)
      commit('setCompletedDocumentsTutorial', null)
      commit('setCompletedTransactionsTutorial', null)
      await router.push({ name: 'Login' })
    },
    async getUserStatus({ commit }, payload) {
      try {
        const userData = await UserService.getUserStatus(payload.email)
        commit('setUserInfo', userData)
      } catch (e) {
        return 'something didn\'t work'
      }
    },
    async becomeCompanyType({ commit }, payload) {
      try {
        const userInfo = UserService.becomeCompanyType(payload.id, payload.type)
        if (userInfo) commit('setUserInfo', userInfo.data)
        await router.push('/transactions')
      } catch (e) {
        console.error('e', e)
        return 'something didn\'t work'
      }
    },
    async updateUserTutorial({ dispatch, state, commit }, payload) {
      try {
        await UserService.updateUserTutorial(payload.id, payload.tutorialCompleted, state.completedTransactionsTutorial, state.completedDocumentsTutorial)
      } catch (e) {
        console.error('error updating user')
      } finally {
        await dispatch('getUserStatus', { email: state.userInfo?.emailAddress })

        if (payload.tutorialCompleted === false) {
          commit('setCompletedDocumentsTutorial', false)
          commit('setCompletedTransactionsTutorial', false)
        }
      }
    }
  }
}
