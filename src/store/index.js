import { createStore } from 'vuex'
import auth from './auth'
import createPersistedState from 'vuex-persistedstate'

const userState = createPersistedState({
  paths: ['auth.userAuth', 'auth.userInfo', 'auth.completedTransactionsTutorial', 'auth.completedDocumentsTutorial']
})

export default createStore({
  strict: true,
  modules: {
    auth
  },
  plugins: [userState],
})
