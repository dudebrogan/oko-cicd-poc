import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import ElementPlus from 'element-plus'
import store from './store'
import i18n from './i18n'
import VueSidebarMenu from 'vue-sidebar-menu'
import './scss/base.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faList, faFileAlt, faColumns, faCartPlus, faClipboardList, faShip, faTruck, faCloudUploadAlt, faCheck, faSpinner, faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueTour from 'v3-tour'

require('v3-tour/dist/vue-tour.css')

library.add(faList)
library.add(faFileAlt)
library.add(faColumns)
library.add(faCartPlus)
library.add(faClipboardList)
library.add(faShip)
library.add(faTruck)
library.add(faCloudUploadAlt)
library.add(faCheck)
library.add(faSpinner)
library.add(faCloudDownloadAlt)

createApp(App).component('font-awesome-icon', FontAwesomeIcon).use(VueTour).use(VueSidebarMenu).use(i18n).use(ElementPlus).use(store).use(router).mount('#app')

